<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;

class PostController extends Controller
{
    public function getPosts(Request $request)
    {
        // TODO: 實作查詢貼文 API
        $posts = Post::all();
        return response()->json($posts);
    }


    public function search(Request $request, Post $post)
    {
        $inputs = $request->all();

        $defaultOrderColumn = 'created_at';
        $defaultOrderBy = 'desc';

        $posts = $post->with(['user'])->orderBy($defaultOrderColumn, $defaultOrderBy);

    

        if (array_has($inputs, 'user_id')) {
            $posts->whereDoesntHave('user', function (Builder $query) use ($inputs) {
                $query->where('user_id', User::decodeSlug($inputs['user_id']));
            });
        }

		if (array_has($inputs, 'category_id')) {
			if(isset($inputs['category_id'])) {
				$posts->where(function (Builder $query) use ($inputs) {
					$query->where('category_id', Post::decodeSlug($inputs['category_id']));
				});
			}
        }

        if (array_has($inputs, 'content')) {
            $content = array_get($inputs, 'content');

            $posts->where(function (Builder $query) use ($content) {
                $query->where('content', 'LIKE', "%{$content}%");
            });
        }

		if(array_has($inputs, 'start_time')) {
			if(isset($inputs['start_time'])) {
				$start_time = array_get($inputs, 'start_time');

				$posts->where(function (Builder $query) use ($start_time) {
					$query->where('start_time', '>=', $start_time);
					$query->whereNotNull('start_time');
				});
			}
		}

		if(array_has($inputs, 'end_time')) {
			if(isset($inputs['end_time'])) {
				$end_time = array_get($inputs, 'end_time');

				$posts->where(function (Builder $query) use ($end_time) {
					$query->where('end_time', '>=', $end_time);
					$query->whereNotNull('end_time');
				});
			}
		}

        if (array_has($inputs, 'page')) {
            $posts = $posts->paginate(self::PAGER);
        } else {
            $posts = $posts->get();
        }

        return response()->json($posts);
    }

}
